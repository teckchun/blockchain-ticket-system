'use strict'

const { Contract } = require('fabric-contract-api');

// import model
let Customer = require('./models/Customer');
let Merchant = require('./models/Merchant');
let MerchantType = require('./models/MerchantType');
let Ticket = require('./models/Ticket');


class TicketContract extends Contract {

    // Init
    /**
     * 
     * @param {*} ctx 
     */
    async initLedger(ctx){
        let cus1 = await new User('John','PhnomPenh','010-999-999','john','123456','customer');
        let merchant1 = await new User('Kim','Korea','010-989-989','kim','123456','partner');

        const ticket = [
           {
               'value':100,
               'price':100,
               'code' : 'TK001',
               'issueDate': Date.now(),
               'expiryDate': '2019-02-05',
               'issueBy': merchant1,
               'owner': cus1,
           },
           {
                'value':50,
                'price':50,
                'code' : 'TK002',
                'issueDate': Date.now(),
                'expiryDate': '2019-02-05',
                'issueBy': merchant1,
                'owner': cus1,
            },
            {
                'value':200,
                'price':200,
                'code' : 'TK003',
                'issueDate': Date.now(),
                'expiryDate': '2019-02-05',
                'issueBy': partner1,
                'owner': cus1,
            }
        ];

        for(let i=0; i<ticket.length; i++){
            await ctx.stub.putState(ticket[i].ticketId,Buffer.from(JSON.stringify(ticket[i])));
            console.info('Added new ticket', ticket[i]);

        }
    }
    //Ticket
    /**
     * 
     * @param {*} ctx 
     * @param {*} value 
     * @param {*} price 
     * @param {*} code 
     * @param {*} issueDate 
     * @param {*} expiryDate 
     * @param {*} issueBy 
     * @param {*} owner 
     */
    async issueTicket(ctx,value,price,code,issueDate,expiryDate,issueBy,owner){
        
        let date = new Date();
        expiryDate =  new Date(+date + 30 *86400000) 
        const ticket = {
            value,
            price,
            code,
            issueDate: Date.now(),
            expiryDate: expiryDate,
            issueBy,
            owner
        }

        await ctx.stub.putState(ticket.ticketId,Buffer.from(JSON.stringify(ticket)));
        
    }
    /**
     * 
     * @param {*} ctx 
     * @param {*} ticketId 
     * @param {*} newOwner 
     */
    async saleTicket(ctx,ticketId,newPrice,newOwner){

        const ticketAsBytes = await ctx.stub.getState(ticketId); // get ticket from chaincode
        if(!ticketAsBytes || ticketAsBytes.length ==0){
            throw new Error(`${ticketId} does not exist.`);
        }
        const ticket = JSON.parse(ticketAsBytes.toString());
        ticket.owner = newOwner;
        ticket.price = newPrice;
        await ctx.stub.putState(ticketId, Buffer.from(JSON.stringify(ticket)));
    }
    // Customer
    async createCustomer(ctx,args){
        const customer = await new Customer(
            args.cusId,
            args.cusName,
            args.address,
            args.phone,
            args.userName,
            args.password);
        // save to blockchain
        await ctx.stub.putState(customer.cusId,Buffer.from(JSON.stringify(customer)));
        
        console.info('============= END : Create Customer ===========');
        let response = `customer with cusId ${customer.cusId} is updated in the world state`;
        return response;
    } 
    // Merchant
    async createMerchant(ctx,args){
        const merchant = await new Merchant(
            args.merchantId,
            args.merchantName,
            args.type,
            args.address,
            args.phone,
            args.userName,
            args.password);
        // save to blockchain
        await ctx.stub.putState(merchant.merchantId,Buffer.from(JSON.stringify(merchant)));
        
        console.info('============= END : Create Merchant ===========');
        let response = `customer with cusId ${merchant.merchantId} is updated in the world state`;
        return response;
    } 
    // Asset Utils
   /**
   *
   * readMyAsset
   *
   * Reads a key-value pair from the world state, based on the key given.
   *  
   * @param myAssetId - the key of the asset to read
   * @returns - nothing - but reads the value in the world state
   */
  async readMyAsset(ctx, myAssetId) {

    const exists = await this.myAssetExists(ctx, myAssetId);

    if (!exists) {
      // throw new Error(`The my asset ${myAssetId} does not exist`);
      let response = {};
      response.error = `The my asset ${myAssetId} does not exist`;
      return response;
    }

    const buffer = await ctx.stub.getState(myAssetId);
    const asset = JSON.parse(buffer.toString());
    return asset;
  }
    /**
     *
     * myAssetExists
     *
     * Checks to see if a key exists in the world state. 
     * @param myAssetId - the key of the asset to read
     * @returns boolean indicating if the asset exists or not. 
     */
    async myAssetExists(ctx, myAssetId) {

        const buffer = await ctx.stub.getState(myAssetId);
        return (!!buffer && buffer.length > 0);

    }
  /**
   *
   * deleteMyAsset
   *
   * Deletes a key-value pair from the world state, based on the key given.
   *  
   * @param myAssetId - the key of the asset to delete
   * @returns - nothing - but deletes the value in the world state
   */
  async deleteMyAsset(ctx, myAssetId) {

    const exists = await this.myAssetExists(ctx, myAssetId);
    if (!exists) {
      throw new Error(`The my asset ${myAssetId} does not exist`);
    }

    await ctx.stub.deleteState(myAssetId);

  }

  // Query Asset Utils

  /**
   * Query and return all key value pairs in the world state.
   *
   * @param {Context} ctx the transaction context
   * @returns - all key-value pairs in the world state
  */
 async queryAll(ctx) {

    let queryString = {
      selector: {}
    };

    let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
    return queryResults;

  }

  /**
     * Evaluate a queryString
     *
     * @param {Context} ctx the transaction context
     * @param {String} queryString the query string to be evaluated
    */
  async queryWithQueryString(ctx, queryString) {

    console.log('query String');
    console.log(JSON.stringify(queryString));

    let resultsIterator = await ctx.stub.getQueryResult(queryString);

    let allResults = [];

    // eslint-disable-next-line no-constant-condition
    while (true) {
      let res = await resultsIterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};

        console.log(res.value.value.toString('utf8'));

        jsonRes.Key = res.value.key;

        try {
          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }

        allResults.push(jsonRes);
      }
      if (res.done) {
        console.log('end of data');
        await resultsIterator.close();
        console.info(allResults);
        console.log(JSON.stringify(allResults));
        return JSON.stringify(allResults);
      }
    }
  }

  /**
  * Query by the main objects in this app: ballot, election, votableItem, and Voter. 
  * Return all key-value pairs of a given type. 
  *
  * @param {Context} ctx the transaction context
  * @param {String} objectType the type of the object - should be either ballot, election, votableItem, or Voter
  */
  async queryByObjectType(ctx, objectType) {

    let queryString = {
      selector: {
        type: objectType
      }
    };

    let queryResults = await this.queryWithQueryString(ctx, JSON.stringify(queryString));
    return queryResults;

  }

    


}

module.exports = TicketContract;