'use strict';

class Customer{

/**
 * 
 * @param {*} cusId 
 * @param {*} cusName 
 * @param {*} address 
 * @param {*} phone 
 * @param {*} userName 
 * @param {*} password 
 */

 constructor(cusId,cusName,address,phone,userName,password){
     this.cusId = cusId;
     this.cusName = cusName;
     this.address = address;
     this.phone = phone;
     this.userName = userName;
     this.password = password;
     if(this.__isContract){
        delete this.__isContract;
     }
     return this;
 }
}

module.exports = Customer;