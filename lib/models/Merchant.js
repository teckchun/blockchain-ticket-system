'use strict';

class Merchant{

    /**
     * 
     * @param {*} merchantId 
     * @param {*} merchantName 
     * @param {*} type 
     * @param {*} address 
     * @param {*} phone 
     * @param {*} userName 
     * @param {*} password 
     */
    constructor(merchantId,merchantName,type,address,phone,userName,password){
        this.merchantId = merchantId;
        this.issuerName = merchantName;
        this.type = type;
        this.address = address;
        this.phone = phone;
        this.userName = userName;
        if (this.__isContract) {
            delete this.__isContract;
        }
        return this;
    }
}

module.exports = Merchant;