'use strict';

class Ticket {

    /**
     * 
     * @param {*} ticketId 
     * @param {*} type 
     * @param {*} value 
     * @param {*} price 
     * @param {*} code 
     * @param {*} description 
     * @param {*} expiryDate 
     * @param {*} issueBy 
     * @param {*} owner 
     */
     constructor(ticketId,type = 1,value,price,code,description,expiryDate,issueBy,owner){
         this.ticketId = ticketId;
         this.type = type
         this.value = value; // original value
         this.price = price;
         this.code = code;
         this.description = description;
         this.issueDate = Date.now();
         this.expiryDate = expiryDate;
         this.issueBy = issueBy;
         this.owner = owner;
         if(this.__isContract){
             delete this.__isContract;
         }
         return this;
     }
}

module.exports = Ticket;
